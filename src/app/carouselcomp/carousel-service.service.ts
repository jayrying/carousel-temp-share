import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CarouselServiceService {
  config;
  images = [];
  figure = null;
  theta = 0;
  gap = '';
  bfc = false;
  currImage = 0;
  init = false;
  @Output() initAWK = new EventEmitter();

  constructor() { }


  carousel(root, config) {
    this.config = config;
    this.figure = root.querySelector('figure');
    this.images = this.figure.children;
    let n = this.images.length;
    this.gap = this.config.imagePadding || '0px';
    this.bfc = 'bfc' in root.dataset;
    this.theta = 2 * Math.PI / n;
    this.currImage = 0;

    this.setupCarousel(n, parseFloat(getComputedStyle(this.images[0]).width));


  }

  setupCarousel(n, s) {

    let apothem = s / (2 * Math.tan(Math.PI / n));

    this.figure.style.transformOrigin = '50% 50% ' + -apothem + 'px';

    for (let i = 0; i < n; i++)
    {
      this.images[i].style.padding = this.gap;
    }
    for (let i = 1; i < n; i++) {
      this.images[i].style.transformOrigin = '50% 50% ' + -apothem + 'px';
      this.images[i].style.transform = 'rotateY(' + i * this.theta + 'rad)';
    }
    if (this.bfc){
      for (let i = 0; i < n; i++) {
        this.images[i].style.backfaceVisibility = 'hidden';
      }
    }

    this.rotateCarousel(this.currImage,true);
  }

  showNext() {
    if(this.init) {
      this.currImage++;
      this.rotateCarousel(this.currImage, false);
    } else {
      // alert('wait for the carousel to init');
    }
  }

  showPrev() {
    if(this.init) {
      this.currImage--;
      this.rotateCarousel(this.currImage, false);
    } else {
      // alert('wait for the carousel to init');
    }
  }

  rotateCarousel(imageIndex, init) {
    this.figure.style.transform = 'rotateY(' + imageIndex * -this.theta + 'rad)';
    this.figure.style['transition-duration'] = (2000/this.config.loopSpeed) + 'ms';
    if(init) { this.init = true; this.initAWK.emit(true); }
  }


}
