import { Component, OnInit, Input, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { CarouselServiceService } from './carousel-service.service';

@Component({
  selector: 'app-carouselcomp',
  templateUrl: './carouselcomp.component.html',
  styleUrls: ['./carouselcomp.component.css']
})
export class CarouselcompComponent implements OnInit {

  @Input() config;
  @ViewChildren("imgs") imageEles;
  @ViewChild("carousel") carousel;
  @ViewChild("nav") nav;


  images = [];
  waiting = true;
  images_loaded = 0;

  constructor(
    private _CarouselServiceService : CarouselServiceService
  ) { }

  ngAfterViewInit() {
    this.carousel.nativeElement.style.height = 'calc(' + this.config.imageHeight + ' + 60px)';
    this.nav.nativeElement.style.top = 'calc(' + this.config.imageHeight + ' + 30px)';
    this.imageEles._results.map((element,i) => {
      this.imageEles._results[i].nativeElement.style.width = this.config.imageWidth;
      this.imageEles._results[i].nativeElement.style.height = this.config.imageHeight;
      this.imageEles._results[i].nativeElement.style['background-color'] = this.config.imageBG;
    });

  }
  ngOnInit() {
    this.bootstrap(this.config);
    this._CarouselServiceService.initAWK.subscribe((awk) => {
      this.waiting = !awk;
    });
  }

  bootstrap(config) {
    this.images = config.items;
    setTimeout(() => {
      let carousels = document.querySelectorAll('.carousel');
      this._CarouselServiceService.carousel(carousels[0], config);
    },500);
  }

  onLoad() {
    if(this.config.loop) {
      if((this.images.length - 1) === this.images_loaded++)
      {
        setInterval(()=>{
          if(window.document.hasFocus())
          {
            this.showNext();
          }
        },(2000/this.config.loopSpeed));
      }
    }
  }

  showNext() {
    this._CarouselServiceService.showNext();
  }

  showPrev() {
    this._CarouselServiceService.showPrev();
  }

}
