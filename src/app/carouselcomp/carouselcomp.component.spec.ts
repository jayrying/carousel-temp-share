import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarouselcompComponent } from './carouselcomp.component';

describe('CarouselcompComponent', () => {
  let component: CarouselcompComponent;
  let fixture: ComponentFixture<CarouselcompComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarouselcompComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarouselcompComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
