import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  localConfig = {
    loop : true,
    loopSpeed : 2.5,
    imageHeight : '500px',
    imageWidth : '650px',
    imageBackground : 'rgba(0,0,0,0.5)',
    imagePadding : '0px',
    items: [
      {
        image_url : 'https://picsum.photos/800/533?v=111101'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111102'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111103'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111104'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111105'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111106'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111107'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111108'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=111109'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111010'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111011'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111012'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111013'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111014'
      },
      /* {
        image_url : 'https://picsum.photos/800/533?v=1111015'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111016'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111017'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111018'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111019'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111020'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111021'
      },
      {
        image_url : 'https://picsum.photos/800/533?v=1111022'
      } */
    ]
  };


  constructor() {}
}
