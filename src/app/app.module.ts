import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CarouselcompComponent } from './carouselcomp/carouselcomp.component';
import { CarouselServiceService } from './carouselcomp/carousel-service.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent,
    CarouselcompComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [CarouselServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
